(ns k16.crpc.connection.multiplexer.core
  (:require [k16.crpc.util :as util]
            [k16.crpc.connection.core :as connection]
            [k16.crpc.connection.impl.chan.core :as conn.chan]
            #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])))

(defrecord Multiplexer [connection connections]
  connection/IConnection
  (source [_] (connection/source connection))
  (sink [_] (connection/sink connection))
  (close-status [_] (connection/close-status connection))
  connection/ISnapShotable
  (snapshot [_] (connection/snapshot connection))
  connection/IDuplexStream
  (connected? [_] (connection/connected? connection))
  (close! [_ code data]
    (connection/close! connection code data)))

(defn connection->multiplexer
  "Given a `k16.crpc.connection.core/IConnection` returns a multiplexer capable of
  deriving new connections multiplexed over the first"
  [conn]
  (let [connections (a/chan)]
    (a/go-loop []
      (when-let [other (a/<! (connection/source conn))]
        (a/>! connections other)
        (recur)))
    (Multiplexer. conn connections)))

(defn connection
  "Derive a new 'virtual' connection from a multiplexer"
  [conn params]
  (let [[v1 v2] (conn.chan/pair (assoc params :id (util/generate-uuid))
                                (assoc params :id (util/generate-uuid)))]
    (a/put! (connection/sink conn) v2)
    (connection/auto-close-target! conn v1)
    (connection/auto-close-target! conn v2)
    v1))

(comment

 (a/go
   (let [[a b] (conn.chan/pair {} {})
         m1 (connection->multiplexer a)
         m2 (connection->multiplexer b)
         mm1 (connection->multiplexer m1)
         mm2 (connection->multiplexer m2)

         conn-mm1 (connection mm1 {})
         server-mm2 (a/<! (:connections mm2))]
     (a/>! (connection/sink conn-mm1) :aa)
     (assert (= :aa (a/<! (connection/source server-mm2))))

     (connection/close! a :closed nil)
     (a/<! (a/timeout 20))
     (assert (= false (connection/connected? b)))
     (assert (= false (connection/connected? m1)))
     (assert (= false (connection/connected? m2)))
     (assert (= false (connection/connected? mm1)))
     (assert (= false (connection/connected? mm2)))
     ))

 (a/go
   (let [[client server] (conn.chan/pair {} {})
         mux-1 (connection->multiplexer client)
         mux-2 (connection->multiplexer server)
         conn-1 (connection mux-1 {})
         server-1 (a/<! (:connections mux-2))]
     (assert (= true (connection/connected? mux-1)))
     (connection/close! mux-1 :close nil)
     (a/<! (a/timeout 20))
     (assert (= false (connection/connected? conn-1)))
     (assert (= false (connection/connected? mux-1)))
     (assert (= false (connection/connected? mux-2)))
     (assert (= false (connection/connected? server-1))))

   (let [[client server] (conn.chan/pair {} {})
         client-multiplexer (connection->multiplexer client)
         server-multiplexer (connection->multiplexer server)]

     (let [client-connection-a (connection client-multiplexer {})
           server-connection-a (a/<! (:connections server-multiplexer))]

       (a/>! (connection/sink client-connection-a) :aa)
       (assert (= :aa (a/<! (connection/source server-connection-a))))

       (a/>! (connection/sink server-connection-a) :ab)
       (assert (= :ab (a/<! (connection/source client-connection-a)))))

     (let [client-connection-b (connection client-multiplexer {})
           server-connection-b (a/<! (:connections server-multiplexer))]

       (a/>! (connection/sink client-connection-b) :ba)
       (assert (= :ba (a/<! (connection/source server-connection-b))))

       (a/>! (connection/sink server-connection-b) :bb)
       (assert (= :bb (a/<! (connection/source client-connection-b))))

       (a/>! (connection/sink client-connection-b) :bc)
       (assert (= :bc (a/<! (connection/source server-connection-b))))

       (a/>! (connection/sink server-connection-b) :bd)
       (assert (= :bd (a/<! (connection/source client-connection-b))))))))
