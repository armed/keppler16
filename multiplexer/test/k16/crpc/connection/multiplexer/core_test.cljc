(ns k16.crpc.connection.multiplexer.core-test
  (:require [k16.crpc.connection.multiplexer.core :as multiplexer]
            #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])
            #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])
            [k16.testing :as testing]
            [k16.crpc.connection.impl.chan.core :as chan]
            [k16.crpc.connection.core :as connection]))

(t/deftest multiplexer-test
  (let [[client server] (chan/pair {} {})
        client-multiplexer (multiplexer/connection->multiplexer client)
        server-multiplexer (multiplexer/connection->multiplexer server)]
    (t/testing "closing client connection"
      (testing/go-fail-on-timeout
       1000
       (let [client-connection (multiplexer/connection client-multiplexer {})
             server-connection (a/<! (:connections server-multiplexer))]
         (t/is (satisfies? connection/IDuplexStream client-connection))
         (t/is (satisfies? connection/ISnapShotable client-connection))
         (t/is (satisfies? connection/IDuplexStream server-connection))
         (t/is (satisfies? connection/ISnapShotable server-connection))

         (t/is (connection/connected? client-connection))
         (t/is (connection/connected? server-connection))

         (connection/close! client-connection :closed nil)
         (t/is (not (connection/connected? client-connection)))
         (a/<! (a/timeout 10))
         (t/is (not (connection/connected? server-connection))))))

    (t/testing "message flow"
      (testing/go-fail-on-timeout
       1000
       (let [client-connection (multiplexer/connection client-multiplexer {})
             server-connection (a/<! (:connections server-multiplexer))]

         (a/>! (connection/sink client-connection) :x)
         (t/is (= :x (a/<! (connection/source server-connection))))

         (a/>! (connection/sink server-connection) :a)
         (t/is (= :a (a/<! (connection/source client-connection))))

         (a/>! (connection/sink client-connection) :y)
         (t/is (= :y (a/<! (connection/source server-connection))))

         (a/>! (connection/sink server-connection) :b)
         (t/is (= :b (a/<! (connection/source client-connection)))))))))
