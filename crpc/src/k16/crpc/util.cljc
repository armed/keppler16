(ns k16.crpc.util
  (:require #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])))

(defn with-tap [source]
  (let [out (a/chan)
        delayed-source (a/chan)
        m (a/mult delayed-source)]
    (a/tap m out)
    (a/pipe source delayed-source)
    [out m]))

(defn generate-uuid []
  #?(:clj (.toString (java.util.UUID/randomUUID))
     :cljs (random-uuid)))
