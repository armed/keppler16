(ns k16.crpc.connection.core-test
  (:require #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])
            #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])
            [k16.crpc.connection.core :as connection]))

(t/deftest source->source!
  (let [a (connection/connection {})
        b (connection/connection {})]
    (connection/source->source! a b)
    (a/put! (connection/source a) :x)
    (t/is (= :x (a/<!! (connection/source b))))))

(t/deftest source->sink!
  (let [a (connection/connection {})
        b (connection/connection {})]
    (connection/source->sink! a b)
    (a/put! (connection/source a) :x)
    (t/is (= :x (a/<!! (connection/sink b))))))

(t/deftest sink->sink!
  (let [a (connection/connection {})
        b (connection/connection {})]
    (connection/sink->sink! a b)
    (a/put! (connection/sink a) :x)
    (t/is (= :x (a/<!! (connection/sink b))))))

(t/deftest sink->source!
  (let [a (connection/connection {})
        b (connection/connection {})]
    (connection/sink->source! a b)
    (a/put! (connection/sink a) :x)
    (t/is (= :x (a/<!! (connection/source b))))))

(t/deftest connect!-test
  (let [a (connection/connection {})
        b (connection/connection {})]
    (connection/connect! a b)
    (a/put! (connection/sink b) :x)
    (t/is (= :x (a/<!! (connection/source a))))
    (a/put! (connection/sink a) :x)
    (t/is (= :x (a/<!! (connection/source b))))))

(t/deftest on-close-test
  (let [conn (connection/connection {})]
    (a/<!!
     (a/go
       (let [status (connection/on-close conn identity)]
          (connection/close! conn :a "a")
          (t/is (= (a/<! status) {:code :a
                                  :data "a"})))))))

(t/deftest auto-close-target!
  (let [a (connection/connection {})
        b (connection/connection {})]
    (a/<!!
     (a/go
       (connection/auto-close-target! a b)
       (connection/close! a :x "x")
       (t/is (= {:code :x :data "x"} (a/<! (connection/close-status b))))))))
