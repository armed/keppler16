(ns k16.testing
  (:require  #?(:clj [clojure.test :as t]
                :cljs [cljs.test :as t :include-macros true])
            #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])))

(defmacro go-fail-on-timeout [ms form]
  `(do
     (let [~'test-execution (first
                             (a/alts!!
                              [(a/go
                                 (a/<! (a/timeout ~ms))
                                 :timeout)
                               (a/go
                                 (try
                                   ~form
                                   (catch Throwable ~'t
                                     ~'t)))]))]
       (when (instance? Throwable ~'test-execution)
         (println ~'test-execution)
         (throw ~'test-execution))

       (t/is (not= :timeout ~'test-execution)))))
