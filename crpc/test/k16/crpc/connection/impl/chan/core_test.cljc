(ns k16.crpc.connection.impl.chan.core-test
  (:require [k16.crpc.connection.impl.chan.core :as chan]
            #?(:clj [clojure.test :as t]
               :cljs [cljs.test :as t :include-macros true])
            [k16.crpc.connection.core :as connection]
            #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])))


(t/deftest auto-close-target-test
  (let [c (connection/connection {})]
    (t/is (connection/connected? c))
    (connection/close! c :closed nil)
    (t/is (not (connection/connected? c)))))

(t/deftest pair-test
  (let [[a b] (chan/pair {} {})]

    (t/testing "a sink -> b source"
      (a/put! (:sink a) :x)
      (t/is (= :x (a/<!! (:source b)))))

    (t/testing "b sink -> a source"
      (a/put! (:sink b) :x)
      (t/is (= :x (a/<!! (:source a)))))

    (t/testing "a source -> a source"
      (a/put! (:source a) :x)
      (t/is (= :x (a/<!! (:source a)))))

    (t/testing "b source -> b source"
      (a/put! (:source b) :x)
      (t/is (= :x (a/<!! (:source b)))))))
