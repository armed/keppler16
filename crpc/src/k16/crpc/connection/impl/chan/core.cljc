(ns k16.crpc.connection.impl.chan.core
  (:require [k16.crpc.connection.core :as connection]))

(defn pair [a-args b-args]
  (let [a (connection/connection a-args)
        b (connection/connection b-args)]
    (connection/sink->source! a b)
    (connection/sink->source! b a)
    (connection/auto-close-target! a b)
    (connection/auto-close-target! b a)
    [a b]))
