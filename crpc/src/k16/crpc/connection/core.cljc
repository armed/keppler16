(ns k16.crpc.connection.core
  (:require #?(:clj [clojure.core.async :as a]
               :cljs [cljs.core.async :as a])
            [k16.crpc.util :as util]))

(defprotocol ISnapShotable
  (snapshot [shapshotable]))

(defprotocol IDuplexStream
  "One end of a point to point connection"
  (close! [connection code data])
  (connected? [connection]))

(defprotocol IConnection
  (source [connection])
  (sink [connection])
  (close-status [connection]))

(def disconnect-codes
  [::disconnected
   ::forced-disconnected])

(defrecord Connection [id source sink close-status metadata]
  IConnection
  (source [_] source)
  (sink [_] sink)
  (close-status [_] close-status)

  ISnapShotable
  (snapshot [connection]
    (-> connection
        (dissoc :source :sink :mult)
        (update :close-status a/poll!)))

  IDuplexStream
  (connected? [_]
    (nil? (a/poll! close-status)))
  (close! [_ code data]
    (a/put! close-status {:code code :data data})
    (a/close! sink)
    (a/close! source)))

(defn connection [{:keys [id metadata source sink]
                   :or {metadata {}
                        source (a/chan)
                        sink (a/chan)
                        id (util/generate-uuid)}}]
  (map->Connection {:id id
                    :source source
                    :sink sink
                    :close-status (a/promise-chan)
                    :metadata metadata}))

(defn source->source! [a b]
  (a/pipe (source a) (source b) false))

(defn sink->sink! [a b]
  (a/pipe (sink a) (sink b) false))

(defn source->sink! [a b]
  (a/pipe (source a) (sink b) false))

(defn sink->source! [a b]
  (a/pipe (sink a) (source b) false))

(defn connect! [a b]
  (sink->source! b a)
  (sink->source! a b))

(defn on-close
  ([conn f] (on-close conn f (a/promise-chan)))
  ([conn f cancel]
   (a/go
     (let [close-status (close-status conn)
           [val port] (a/alts! [close-status cancel])]
       (condp = port
         cancel nil
         close-status (f val))))))

(defn auto-close-target!
  ([a target] (auto-close-target! a target (a/promise-chan)))
  ([a target cancel]
   (on-close a (fn [{:keys [code data]}]
                 (close! target code data))
             cancel)))

(defn auto-close-both!
  ([a b] (auto-close-both! a b (a/promise-chan)))
  ([a b cancel]
   (auto-close-target! a b cancel)
   (auto-close-target! b a cancel)))
